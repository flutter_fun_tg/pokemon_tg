import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:pokemon_shibuka/main.dart';

void main() {
  testWidgets('Pokemon test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());

    // Find pokemon    
    expect(find.text('Pokemon'), findsOneWidget);    
  });
}
